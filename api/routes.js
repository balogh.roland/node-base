const express = require('express');

const router = express.Router();

// Middlewares

// Controllers

// Healthcheck - test/dummy endpoint
router.get('/healthcheck', (req, res) => res.json({
  uptime: `${Math.floor(process.uptime())} seconds`,
  success: true,
}));

module.exports = router;
