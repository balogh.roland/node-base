const supertest = require('supertest');
const app = require('../app');

const request = supertest(app);

global.Request = request;
global.expect = require('chai').expect;

before(() => app);
