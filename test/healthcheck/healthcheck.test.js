describe('GET /api/healthcheck', () => {
  it('should return a json with the uptime', async () => {
    const res = await Request
      .get('/api/healthcheck')
      .set('Content-Type', 'application/json');

    expect(res.body).to.be.an('object');
    expect(res.body).to.have.all.keys('uptime', 'success');
  });
});
